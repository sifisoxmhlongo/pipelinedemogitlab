package com.gitlab.cphyso.pipeline.drivers;

import com.gitlab.cphyso.pipeline.util.GLOBAL_VARS;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.Scanner;

public class DriverFactory {

     static WebDriver driver;
    // 1. Create a thread safe webdriver instance
    public static ThreadLocal<WebDriver> webDriver = new ThreadLocal<>();
    // 2. static method that checks if the webdriver is initialized
    //    if not it does so and return the webdriver
    public static WebDriver getDriver() throws MalformedURLException {
        if(webDriver.get() == null){
            webDriver.set(createDriver());
        }
        return webDriver.get();
    }

    static WebDriver createDriver() throws MalformedURLException {

        if(getBrowserType().equals("chrome")){

            WebDriverManager.chromedriver().setup();
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--headless");
            driver = new ChromeDriver(options);

        } else if (getBrowserType().equals("gitlab-ci.yml")){

            String seleniumHost = System.getenv("SELENIUM_HOST");
            if (seleniumHost == null) {
                throw new RuntimeException("SELENIUM_HOST environment variable is not set");
            }

            System.out.println("SELENIUM_HOST: " + seleniumHost);
            ChromeOptions option = new ChromeOptions();
            option.addArguments("--headless");
            driver = new RemoteWebDriver(new URL("http://"+seleniumHost+":4444/wd/hub"), option);

        }else if (getBrowserType().equals("docker-compose")){

             String seleniumHost = System.getenv("SELENIUM_HOST");
             if (seleniumHost == null) {
                 throw new RuntimeException("SELENIUM_HOST environment variable is not set");
             }

             System.out.println("SELENIUM_HOST: " + seleniumHost);
             ChromeOptions option = new ChromeOptions();
             option.addArguments("--headless");
             driver = new RemoteWebDriver(new URL("http://"+seleniumHost+":4444/wd/hub"), option);

        }else if(getBrowserType().equals("selenium server")){
            // Below code does not use SELENIUM variable
           URL connectionURL = new URL("http://localhost:4444/wd/hub");
           ChromeOptions option = new ChromeOptions();
           option.addArguments("--headless");
           driver = new RemoteWebDriver(connectionURL, option);
           System.out.println("<<<<<<<<<<<<<<<< running on server ");

        }
        return driver;
    }

    public static void cleanupDriver() {
        webDriver.get().quit();
        webDriver.remove();
    }

    private static String getBrowserType() {

        System.out.println("<<<<<<<<<<<<<<<< running on get browser type method ");

        //        String browserType_remoteValue = System.getProperty("browserType");
//
//        try{
//            if(browserType_remoteValue == null || browserType_remoteValue.isEmpty()){
//                Properties properties = new Properties();
//                FileInputStream file = new FileInputStream(GLOBAL_VARS.propFilePath);
//                properties.load(file);
//                browserType = properties.getProperty("browser").toLowerCase().trim();
//
//            }else{
//                browserType = browserType_remoteValue;
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        return "chome";
    }
}
