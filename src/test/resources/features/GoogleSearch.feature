Feature: Google search

  Scenario: Attempt a google search
    Given user is on google home page
    And user enters cars images in search field
    When user clicks on the search icon
    Then car images are displayed

