package com.gitlab.cphyso.pipeline.steps;

import com.gitlab.cphyso.pipeline.drivers.DriverFactory;
import com.gitlab.cphyso.pipeline.repo.TakeAlotHomePage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.annotations.Test;

import java.io.IOException;

public class TakeAlotTests {

    TakeAlotHomePage user;

    public TakeAlotTests(TakeAlotHomePage user) {
        this.user = user;
    }

    @Test
    @Given("user enters Takealot url")
    public void user_enters_takealot_url() throws IOException {
        user.getDriver().get("https://google.com");
        System.out.println("METHOD 5");
    }

    @Test
    @Given("user clicks enters")
    public void user_clicks_enters() throws IOException {
        user.sendKeys(By.name("q"),"ai generation");
        System.out.println("METHOD 6");
    }

    @Test
    @Then("user is re directed to Takealot")
    public void user_is_re_directed_to_takealot()  throws IOException {
        user.getDriver().findElement(By.name("q")).sendKeys(Keys.RETURN);
        System.out.println("METHOD 7");

        DriverFactory.cleanupDriver();
    }

}
