package com.gitlab.cphyso.pipeline.steps;

import com.gitlab.cphyso.pipeline.drivers.DriverFactory;
import com.gitlab.cphyso.pipeline.repo.GoogleHomePO;
import com.gitlab.cphyso.pipeline.util.GLOBAL_VARS;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.annotations.Test;
import org.openqa.selenium.remote.tracing.opentelemetry.OpenTelemetryTracer;


import java.io.IOException;

public class GoogleSearchTests {

    public GoogleHomePO user;

    public GoogleSearchTests(GoogleHomePO user) {
        this.user = user;
    }

    @Test
    @Given("user is on google home page")
    public void user_is_on_google_home_page() throws IOException {
        user.navigateTo_URL("https://google.com");

        System.out.println("METHOD 1");
    }

    @Test
    @Given("user enters cars images in search field")
    public void user_enters_cars_images_in_search_field() throws IOException {
        user.sendKeys(By.name("q"),"car images");
        System.out.println("METHOD 2");
    }

    @Test
    @When("user clicks on the search icon")
    public void user_clicks_on_the_search_icon() throws IOException {
      user.getDriver().findElement(By.name("q")).sendKeys(Keys.RETURN);
      System.out.println("METHOD 3");
    }

    @Then("car images are displayed")
    public void car_images_are_displayed() throws IOException {
        DriverFactory.cleanupDriver();
        System.out.println("METHOD 4");
    }

}
