package com.gitlab.cphyso.pipeline.runners;




import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.DataProvider;


@CucumberOptions(
        features = {"src/test/resources/features/TakeAlot.feature"},
        glue = {"com.gitlab.cphyso.pipeline.steps"},
        plugin = {"json:target/cucumber.json", "html:target/site/cucumber-pretty.html"}
)
public class TakeAlotTestRunner extends AbstractTestNGCucumberTests{

    @Override
    @DataProvider(parallel = true)
    public Object[][] scenarios() {
        return super.scenarios();
    }
}
